# Clingo Creek Visualization

This Webpage can visualize the output of clingo that solved a creek puzzle.

This can be helpful when trying to find a solution for solving creek with clingo to help understand what answer sets are being produced.

this webpage expects the console output of clingo in the file `out.txt` next to the sketch.js.

You just need to host the webpage with any server (i used vscode and the live server extension).

The script needs data like this in the out file

```
maxX(5) maxY(5) hint(0,4,0) hint(1,1,3) hint(2,2,0) hint(2,4,3) hint(3,1,3) hint(3,3,1) hint(4,4,1) hint(5,1,1) hint(5,5,1) black(1,1) black(1,2) black(2,1) black(2,4) black(3,1) black(3,4) black(3,5) black(4,1) black(4,2) black(5,1) black(5,5)
```

so add rules to get the max of X and Y so it can read the grid size and also add the hints to the output so i can visualize those too. It only needs the black ones and all others will be rendered white.

```lp
maxX(N) :-  #max{ X : cell(X,_) } = N.
maxY(N) :-  #max{ Y : cell(_,Y) } = N.

#show black/2.
#show maxX/1.
#show maxY/1.
#show hint/3.
```

To get the output for visualization use this command

```bash
clingo creek.lp instance04.lp 0 > /path/to/webpage/clingo-creek-visualization/out.txt
```

## Screenshots

![](pictures/vis1.png)


![](pictures/vis2.png)