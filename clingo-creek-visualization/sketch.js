let fileInput;

let cols = 0;
let rows = 0;

let white = 255;
let black = 0;
let gray = 125;

let size = 70;
let offsetH = 80;
let offsetW = 20;
let shouldDraw = true;
let btn;
let btn2;

let input = [];

let colors = [];
let solution = 0;
let hints = [];

let re = /(\w*)\(([0-9]*),*([0-9]*),*([0-9]*)/;
let re2 = /(% )*(clingo|Reading|Solving|SATISFIABLE|Models|Calls|Time|CPU|Answer|ANSWER|% )/;

function btnClicked() {
  if (solution > 0) {
    solution = solution - 1;
    shouldDraw = true;
  }
}
function btnClicked2() {
  if (solution + 1 < input.length) {
    solution = solution + 1;
    shouldDraw = true;
  }
}

function preload() {
  fileInput = loadStrings("out.txt");
}

function setup() {
  let inc = 0;
  for (let i = 0; i < fileInput.length; i++) {
    if (!fileInput[i].match(re2) && fileInput[i] !== "") {
      input[inc++] = fileInput[i];
    }
  }

  btn = createButton("<- Prev");
  btn.mouseClicked(btnClicked);
  btn2 = createButton("Next ->");
  btn2.mouseClicked(btnClicked2);

  for (let i = 0; i < input.length; i++) {
    let data = input[i].split(" ");
    for (let j = 0; j < data.length; j++) {
      let out = re.exec(data[j]);
      if (out[1] === "maxX") {
        cols = parseInt(out[2]);
      } else if (out[1] === "maxY") {
        rows = parseInt(out[2]);
      }
    }
  }
  createCanvas(
    Math.max(cols * size + 200, window.innerWidth),
    Math.max(rows * size + 200, window.innerHeight)
  );

  for (let l = 0; l < input.length; l++) {
    colors[l] = [];
    for (let i = 0; i < cols; i++) {
      colors[l][i] = [];
      for (let j = 0; j < rows; j++) {
        colors[l][i][j] = white;
      }
    }
  }

  for (let i = 0; i < input.length; i++) {
    let data = input[i].split(" ");
    for (let j = 0; j < data.length; j++) {
      let out = re.exec(data[j]);
      let color = gray;
      if (out[1] === "black") {
        color = black;
      } else if (out[1] === "white") {
        color = white;
      } else if (out[1] === "hint") {
        hints.push({
          x: parseInt(out[2]),
          y: parseInt(out[3]),
          n: parseInt(out[4]),
        });
        continue;
      } else if (out[1] === "maxY" || out[1] === "maxX") {
        continue;
      }
      let x = parseInt(out[2]) - 1;
      let y = parseInt(out[3]) - 1;
      // ignore cells outside of grid
      if (x < 0 || y < 0 || x >= cols || y >= rows) {
        console.log(out);
        console.log("error index");
        continue;
      }
      colors[i][x][y] = color;
    }
  }
}

function draw() {
  if (shouldDraw) {
    background(50);
    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        let x = i * size + offsetW;
        let y = j * size + offsetH;
        fill(colors[solution][i][j]);
        stroke(125);
        rect(x, y, size, size);
      }
    }
    textSize(24);
    hints.forEach((hint) => {
      for (let i = 0; i <= cols; i++) {
        for (let j = 0; j <= rows; j++) {
          if (hint.x == i && hint.y == j) {
            fill("red");
            let x = i * size + offsetW;
            let y = j * size + offsetH;
            text(hint.n, x - 7, y + 8);
          }
        }
      }
    });
    fill(255);
    textSize(24);
    text(solution + 1, 20, 30);
    text("/", 45, 30);
    text(input.length, 65, 30);

    btn.position(20, 40);
    btn2.position(90, 40);
    shouldDraw = false;
  }
}
